package com.batch.couch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbatchCouchbaseApplication {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(SpringbatchCouchbaseApplication.class, args);
	}
}
