package com.batch.couch;

/**
 * Created by szagoret on 21.02.2017.
 */
public class Reading {
    private String id;
    private String idPdc;
    private String date;
    private String index;
    private String consumption;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPdc() {
        return idPdc;
    }

    public void setIdPdc(String idPdc) {
        this.idPdc = idPdc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    @Override
    public String toString() {
        return "Reading{" +
                "id='" + id + '\'' +
                ", idPdc='" + idPdc + '\'' +
                ", date='" + date + '\'' +
                ", index='" + index + '\'' +
                ", consumption='" + consumption + '\'' +
                '}';
    }
}
