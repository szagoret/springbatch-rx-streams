package com.batch.couch;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.InsertOneModel;
import org.bson.Document;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * Created by szagoret on 21.02.2017.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

//    @Autowired
//    public CouchbaseTemplate couchbaseTemplate;

    @Autowired
    MongoClient mongoClient;

    // tag::readerwriterprocessor[]
    @Bean
    public FlatFileItemReader<Reading> reader() {
        FlatFileItemReader<Reading> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource("reading20.csv"));
        reader.setLineMapper(new DefaultLineMapper<Reading>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[]{"id", "idPdc", "date", "index", "consumption"});
                setDelimiter(";");
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Reading>() {{
                setTargetType(Reading.class);
            }});
        }});
        return reader;
    }

    @Bean
    public ItemProcessor<Reading, JsonDocument> processor() {
        return reading -> {
            reading.setId(UUID.randomUUID().toString());
            JsonObject readingJsonObject = JsonObject.empty();
            readingJsonObject.put("id", reading.getId());
            readingJsonObject.put("idPdc", reading.getIdPdc());
            readingJsonObject.put("date", reading.getDate());
            readingJsonObject.put("index", reading.getIndex());
            readingJsonObject.put("consumption", reading.getConsumption());
            return JsonDocument.create(reading.getId(), readingJsonObject);
        };
    }
//
//    @Bean
//    public ItemWriter<JsonDocument> couchbaseWriter() {
//        return (list) -> {
//            AsyncBucket asyncBucket = couchbaseTemplate.getCouchbaseBucket().async();
//            Observable.from(list).flatMap(readingJsonObject -> asyncBucket.upsert(readingJsonObject)
//                    .retryWhen(RetryBuilder.anyOf(BackpressureException.class)
//                            .delay(Delay.exponential(TimeUnit.MILLISECONDS, 100))
//                            .max(10).build()))
//                    .last()
//                    .toBlocking()
//                    .single();
//        };
//    }


    @Bean
    public ItemProcessor<Reading, InsertOneModel> mongoDbProcessor() {
        return reading -> {
            Document document = new Document();
            document.put("_id", UUID.randomUUID().toString());
            document.put("rowNumber", reading.getId());
            document.put("idPdc", reading.getIdPdc());
            document.put("date", reading.getDate());
            document.put("index", reading.getIndex());
            document.put("consumption", reading.getConsumption());
            InsertOneModel<Document> oneModel = new InsertOneModel<>(document);
            return oneModel;
        };
    }

    @Bean
    public ItemWriter mongoDbWriter() {
        CountDownLatch latch = new CountDownLatch(1);
        return (list) -> {
            MongoDatabase database = mongoClient.getDatabase("smartf");
            MongoCollection<Document> collection = database.getCollection("reading");
            collection.bulkWrite(list, ((result, t) -> {
                System.out.println("Finish write...");
                latch.countDown();
            }));

            latch.await();
            System.out.println("Continue after writing...");
        };
    }

    // end::readerwriterprocessor[]
    // tag::jobstep[]
    @Bean
    public Job importReadingJob() {
        return jobBuilderFactory.get("importReadingJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Reading, InsertOneModel>chunk(10000)
                .reader(reader())
                .processor(mongoDbProcessor())
                .writer(mongoDbWriter())
                .build();
    }
    // end::jobstep[]


}
